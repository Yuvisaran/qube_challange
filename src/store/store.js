import { combineReducers } from "redux";
import reducer from "../service/admin/reducer";
import loginReducer from "../service/login/reducer";

const rootReducer = combineReducers({
  reducer,
  loginReducer
});

export default rootReducer;
