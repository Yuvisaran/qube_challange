import React, { Component } from "react";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";

import DashboardForm from "./components/container/admin/Dashboard";
import Login from "./components/container/common/login";

class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Redirect exact path="/" to="/login" />
          <Route path="/login" component={Login} />
          <Route path="/dashboard" component={DashboardForm} />
        </Switch>
      </BrowserRouter>
    );
  }
}

export default App;
