const actionType = {
    ADDUSER_SUCCESS: 'ADDUSER_SUCCESS',
    EDITUSER_SUCCESS: 'EDITUSER_SUCCESS'
}
export default actionType;