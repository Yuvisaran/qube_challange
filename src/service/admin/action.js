import actionType from "./actionType";
import { toast } from "react-toastify";
import 'react-toastify/dist/ReactToastify.css';

export const addUserInfoService = data => dispatch => {
  dispatch({
    payLoad: data,
    type: actionType.ADDUSER_SUCCESS
  });
  toast.success("User added successfully !!!", {
    position: toast.POSITION.TOP_CENTER
  });
};


export const editUserInfoService = data => dispatch => {
    dispatch({
      payLoad: data,
      type: actionType.EDITUSER_SUCCESS
    });
    toast.success("User updated successfully !!!", {
      position: toast.POSITION.TOP_CENTER
    });
  };
  
