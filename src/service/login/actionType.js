const actionType = {
  LOGIN_SUCCESS: "LOGIN_SUCCESS",
  // LOGIN_FAILURE: "LOGIN_FAILURE",
  LOGOUT_SUCCESS: "LOGOUT_SUCCESS"
};
export default actionType;
