import actionType from "./actionType";

const initialState = {
  loginStatus: false
};
const loginReducer = (state = initialState, action) => {
  switch (action.type) {
    case actionType.LOGIN_SUCCESS:
      return {
        ...state,
        loginStatus: true
      };
    // case actionType.LOGIN_FAILURE:
    //   return {
    //     ...state,
    //     loginStatus: false
    //   };
    case actionType.LOGOUT_SUCCESS:
      return {
        ...state,
        loginStatus: false
      };
    default:
      return state;
  }
};

export default loginReducer;
