import actionType from "./actionType";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

export const loginService = data => dispatch => {
  if (data.userName != "" && data.password == "admin") {
    dispatch({
      payLoad: data,
      type: actionType.LOGIN_SUCCESS
    });
    toast.success("Welcome" + "    " + data.userName + "!!", {
      position: toast.POSITION.TOP_CENTER
    });
  } else {
    // dispatch({
    //   payLoad: data,
    //   type: actionType.LOGIN_FAILURE
    // });
    toast.success("Invalid credential", {
      position: toast.POSITION.TOP_CENTER
    });
  }
};

export const logoutService = () => dispatch => {
  dispatch({
    type: actionType.LOGOUT_SUCCESS
  });
  toast.success("Logged out successfully !!!", {
    position: toast.POSITION.TOP_CENTER
  });
};
