import React, { Component } from "react";
import { connect } from "react-redux";
import { toast, ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import {
  addUserInfoService,
  editUserInfoService
} from "../../../service/admin/action";
import { logoutService } from "../../../service/login/action";

class DashboardForm extends Component {
  state = {
    name: "",
    age: "",
    country: "",
    gender: "",
    errors: {
      name: "",
      age: "",
      country: "",
      gender: ""
    },
    addPopupShow: false,
    update: false,
    deletePopupShow: false,
    listArray: [],
    index: ""
  };

  handleChange = e => {
    const { name, value } = e.target;
    this.setState({ [name]: value });
  };

  restrictSpecialchar(e) {
    const re = /[a-zA-Z]+/g;
    if (!re.test(e.key)) {
      e.preventDefault();
    }
  }

  restrictSpecialcharAlpha(e) {
    const re = /[0-9]+/g;
    if (!re.test(e.key)) {
      e.preventDefault();
    }
  }

  addUserSumbit = e => {
    const { name, age, country, gender } = this.state;
    e.preventDefault();
    if (this.props.addUserInfo !== "") {
      const payLoad = this.props.addUserInfo.concat({
        name,
        age,
        country,
        gender
      });
      this.props.addUserInfoService(payLoad);
    } else {
      const payLoad = [
        {
          name,
          age,
          country,
          gender
        }
      ];
      this.props.addUserInfoService(payLoad);
    }
    this.setState({
      name: "",
      age: "",
      country: "",
      gender: "",
      addPopupShow: false
    });
  };

  editFunction(index, each) {
    this.setState(
      {
        update: true,
        name: each.name,
        age: each.age,
        country: each.country,
        gender: each.gender,
        index: index
      },
      () => {
        if (each.gender === "Male") {
          document.getElementById("maleId").checked = true;
        } else if (each.gender === "Female") {
          document.getElementById("femaleId").checked = true;
        }
      }
    );
  }

  editUserSumbit = e => {
    e.preventDefault();
    const { name, age, country, gender, index } = this.state;
    const { addUserInfo } = this.props;
    addUserInfo[index] = { name, age, country, gender };
    const payLoad = addUserInfo;
    this.props.editUserInfoService(payLoad);
    this.setState({
      name: "",
      age: "",
      country: "",
      gender: "",
      update: false
    });
  };

  delete(key, item) {
    this.props.addUserInfo.splice(key, 1);
    this.setState({ listArray: this.props.addUserInfo }, () => {
      toast.success("User deleted successfully !!!", {
        position: toast.POSITION.TOP_CENTER
      });
    });
  }

  componentWillReceiveProps(newProps) {
    console.log("will[ppe[");
    if (newProps.loginStatus == false) {
      setTimeout(() => {
        this.props.history.push("/");
      }, 2000);
    }
  }

  render() {
    const { name, age, country, gender, addPopupShow, update } = this.state;
    const { addUserInfo } = this.props;

    return (
      <div className="container">
        <ToastContainer />
        <div className="row">
          <h2>
            <b>User Dashboard</b>
          </h2>
          <h4
            style={{ float: "right", cursor: "pointer", marginTop: "auto" }}
            onClick={() => {
              this.props.logoutService();
            }}
          >
            Logout
          </h4>
        </div>
        {/* Table List */}
        <div className="table-wrapper">
          <div className="table-title">
            <div className="row">
              <div className="col-sm-6">
                <h4>User Info</h4>
              </div>
              <div className="col-sm-6">
                <a
                  href="#addEmployeeModal"
                  onClick={() => this.setState({ addPopupShow: true })}
                  className="btn btn-success"
                  data-toggle="modal"
                >
                  <i className="material-icons">&#xE147;</i>{" "}
                  <span>Add New User</span>
                </a>
              </div>
            </div>
          </div>
          <table className="table table-striped table-hover">
            <thead>
              <tr>
                <th>Name</th>
                <th>Age</th>
                <th>Country</th>
                <th>Gender</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {addUserInfo.length !== 0 ? (
                addUserInfo.map((item, i) => {
                  return (
                    <tr key={i}>
                      <td>{item.name}</td>
                      <td>{item.age}</td>
                      <td>{item.country}</td>
                      <td>{item.gender}</td>
                      <td>
                        <a
                          href="#"
                          className="edit"
                          onClick={this.editFunction.bind(this, i, item)}
                        >
                          <i
                            className="material-icons"
                            data-toggle="tooltip"
                            title="Edit"
                          >
                            &#xE254;
                          </i>
                        </a>
                        <a href="#" onClick={this.delete.bind(this, i)}>
                          <i
                            className="material-icons"
                            data-toggle="tooltip"
                            title="Delete"
                          >
                            &#xE872;
                          </i>
                        </a>
                      </td>
                    </tr>
                  );
                })
              ) : (
                <tr>
                  <td colSpan="5" className="text-center">
                    No Records Found...
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
        {/* <!-- Add New User--> */}
        {addPopupShow && (
          <div id="addEmployeeModal" className="modal fade in">
            <div className="modal-dialog">
              <div className="modal-content">
                <form onSubmit={this.addUserSumbit}>
                  <div className="modal-header">
                    <h4 className="modal-title">Add New User</h4>
                    <button
                      type="button"
                      onClick={() =>
                        this.setState({
                          addPopupShow: false,
                          name: "",
                          age: "",
                          country: "",
                          gender: ""
                        })
                      }
                      className="close"
                      data-dismiss="modal"
                      aria-hidden="true"
                    >
                      &times;
                    </button>
                  </div>
                  <div className="modal-body">
                    <div className="form-group">
                      <label>Name</label>
                      <input
                        type="text "
                        name="name"
                        value={name}
                        onKeyPress={e => this.restrictSpecialchar(e)}
                        className="form-control"
                        onChange={this.handleChange}
                      />
                    </div>
                    <div className="form-group">
                      <label>Age</label>
                      <input
                        type="number"
                        name="age"
                        value={age}
                        onKeyPress={e => this.restrictSpecialcharAlpha(e)}
                        className="form-control"
                        onChange={this.handleChange}
                      />
                    </div>
                    <div className="form-group">
                      <label>Country</label>
                      <select
                        name="country"
                        className="form-control"
                        value={country}
                        onChange={this.handleChange}
                      >
                        <option value="">Select Country</option>
                        <option value="India">India</option>
                        <option value="Japan">Japan</option>
                        <option value="Australia">Australia</option>
                      </select>
                    </div>
                    <div className="form-group">
                      <label>Gender</label>
                      <div>
                        <input
                          type="radio"
                          id="mId"
                          name="gender"
                          value="Male"
                          className=""
                          onChange={this.handleChange}
                        />
                        Male <br />
                        <input
                          type="radio"
                          id="fId"
                          name="gender"
                          value="Female"
                          className=""
                          onChange={this.handleChange}
                        />
                        Female
                      </div>
                    </div>
                  </div>
                  <div className="modal-footer">
                    <input
                      type="button"
                      onClick={() =>
                        this.setState({
                          addPopupShow: false,
                          name: "",
                          age: "",
                          country: "",
                          gender: ""
                        })
                      }
                      className="btn btn-default"
                      data-dismiss="modal"
                      value="Cancel"
                    />
                    <input
                      type="submit"
                      className="btn btn-success"
                      value="Add"
                      disabled={
                        name === "" ||
                        age === "" ||
                        country === "" ||
                        gender === ""
                      }
                    />
                  </div>
                </form>
              </div>
            </div>
          </div>
        )}
        {/* <!-- Edit User --> */}
        {update && (
          <div id="addEmployeeModal" className="modal fade in">
            <div className="modal-dialog">
              <div className="modal-content">
                <form onSubmit={this.editUserSumbit}>
                  <div className="modal-header">
                    <h4 className="modal-title">Edit User Info</h4>
                    <button
                      type="button"
                      onClick={() =>
                        this.setState({
                          update: false,
                          name: "",
                          age: "",
                          country: "",
                          gender: ""
                        })
                      }
                      className="close"
                      data-dismiss="modal"
                      aria-hidden="true"
                    >
                      &times;
                    </button>
                  </div>
                  <div className="modal-body">
                    <div className="form-group">
                      <label>Name</label>
                      <input
                        type="text "
                        name="name"
                        value={name}
                        onKeyPress={e => this.restrictSpecialchar(e)}
                        className="form-control"
                        onChange={this.handleChange}
                      />
                    </div>
                    <div className="form-group">
                      <label>Age</label>
                      <input
                        type="number"
                        name="age"
                        value={age}
                        onKeyPress={e => this.restrictSpecialcharAlpha(e)}
                        className="form-control"
                        onChange={this.handleChange}
                      />
                    </div>
                    <div className="form-group">
                      <label>Country</label>
                      <select
                        name="country"
                        className="form-control"
                        value={country}
                        onChange={this.handleChange}
                      >
                        <option value="">Select Country</option>
                        <option value="India">India</option>
                        <option value="Japan">Japan</option>
                        <option value="Australia">Australia</option>
                      </select>
                    </div>
                    <div className="form-group">
                      <label>Gender</label>
                      <div>
                        <input
                          type="radio"
                          id="maleId"
                          name="gender"
                          value="Male"
                          className=""
                          onChange={this.handleChange}
                        />
                        Male <br />
                        <input
                          type="radio"
                          id="femaleId"
                          name="gender"
                          value="Female"
                          className=""
                          onChange={this.handleChange}
                        />
                        Female
                      </div>
                    </div>
                  </div>
                  <div className="modal-footer">
                    <input
                      type="button"
                      onClick={() =>
                        this.setState({
                          update: false,
                          name: "",
                          age: "",
                          country: "",
                          gender: ""
                        })
                      }
                      className="btn btn-default"
                      data-dismiss="modal"
                      value="Cancel"
                    />
                    <input
                      type="submit"
                      className="btn btn-success"
                      value="Save"
                      disabled={
                        name === "" ||
                        age === "" ||
                        country === "" ||
                        gender === ""
                      }
                    />
                  </div>
                </form>
              </div>
            </div>
          </div>
        )}
      </div>
    );
  }
}
const mapStateToProps = state => ({
  addUserInfo: state.reducer.addUserInfo,
  addUserSucc: state.reducer.addUserSucc,
  editUserSucc: state.reducer.editUserSucc,
  loginStatus: state.loginReducer.loginStatus
});
const mapDispatchToProps = dispatch => ({
  addUserInfoService: payLoad => dispatch(addUserInfoService(payLoad)),
  editUserInfoService: payLoad => dispatch(editUserInfoService(payLoad)),
  logoutService: () => dispatch(logoutService())
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(DashboardForm);
