import React, { Component } from "react";
import { connect } from "react-redux";
import { ToastContainer } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";

import { loginService } from "../../../service/login/action";

class Login extends Component {
  state = { userName: "", password: "" };

  handleChange = e => {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value });
  };

  toggleLoginService = e => {
    e.preventDefault();
    const { userName, password } = this.state;
    const payload = {
      userName,
      password
    };
    this.props.loginService(payload);
  };

  componentWillReceiveProps(newProps) {
    if (this.props.loginStatus != newProps.loginStatus) {
      if (newProps.loginStatus == true) {
        setTimeout(() => {
          this.props.history.push("/dashboard");
        }, 2000);
      }
    }
  }

  render() {
    const { userName, password } = this.state;
    return (
      <div id="login">
        <ToastContainer />
        <h3 className="text-center text-white pt-5">QUBE CENIMAS</h3>
        <div className="container">
          <div
            id="login-row"
            className="row justify-content-center align-items-center"
          >
            <div id="login-column" className="col-md-6">
              <div id="login-box" className="col-md-12">
                <form
                  id="login-form"
                  className="form"
                  onSubmit={this.toggleLoginService}
                >
                  <h3 className="text-center text-info">Login</h3>
                  <div className="form-group">
                    <label htmlFor="username" className="text-info">
                      Username:
                    </label>
                    <br />
                    <input
                      type="text"
                      name="userName"
                      id="username"
                      className="form-control"
                      value={userName}
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="form-group">
                    <label htmlFor="password" className="text-info">
                      Password:
                    </label>
                    <br />
                    <input
                      type="password"
                      name="password"
                      id="password"
                      className="form-control"
                      value={password}
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="form-group">
                    <input
                      type="submit"
                      name="submit"
                      className="btn btn-info btn-md"
                      value="submit"
                      disabled={!userName || !password}
                    />
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  loginStatus: state.loginReducer.loginStatus
});

const mapDispatchToProps = dispatch => ({
  loginService: payload => dispatch(loginService(payload))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);
